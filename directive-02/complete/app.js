angular.module('bacon', ['ipsumService'])
    .controller('BaconController', function() {
      this.title3 = 'bacon-ipsum s title';
      this.paragraphs3 = 1;
    })
    .directive('baconIpsum', function(generator) {
      
      return {
        scope:{
          title: '@',
          title2way: '='
        },
        link:function(scope, element, attributes) {

          scope.data = generator.getParagraphs(attributes.paragraphs);

          var deregParagraphs = attributes.$observe("paragraphs", function() {
            scope.data = generator.getParagraphs(attributes.paragraphs);
          });

          scope.$on("$destroy", function() {
            deregParagraphs();
          });

        },
        templateUrl: 'baconIpsum.html'
      };
    });
