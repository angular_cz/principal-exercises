describe('calculator controller', function() {
  var controller;

  beforeEach(module('diApp'));

  beforeEach(inject(function($controller) {

    // TODO 5.1 - vytvořte mock factory calculator
    controller = $controller('CalculatorCtrl', {});
  }));

  // TODO 5.2 - test, který zkontroluje návratovou hodnotu
  // TODO 5.3 - test, který zkontroluje, že je mock volán.

  // TODO 4.1 - testy odstraňte, nyní je testována přímo factory calculator
  it('should return 90 when format is A5 and number of pages is 0', function() {

    controller.product = {
      pageSize: 'A5',
      numberOfPages: 0
    };

    expect(controller.getPrice()).toEqual(90);

  });

  it('should return 120 when format is A5 and number of pages is 50', function() {
    // TODO 3.1 - odstraňte pending
    pending();

    // TODO 3.2 - implementujte test
  });
});