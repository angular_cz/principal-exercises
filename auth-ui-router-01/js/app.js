angular.module('authApp', ['ngResource', 'ui.router', 'ui.bootstrap'])
    .constant('REST_URI', 'http://security-api.angular.cz')

    .config(function($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/');

      $stateProvider
          .state('home', {
            url: "/",
            controller: 'HomeCtrl',
            controllerAs: 'home',
            templateUrl: 'home.html'
          })

          .state('order', {
            abstract: true,
            url: "/order",
            template: '<ui-view/>',
            data: {
              loggedOnly: true
            }
          })

          .state('order.list', {
            url: "/list",
            controller: 'OrderListController',
            controllerAs: 'list',
            templateUrl: 'orderList.html'
          })

          .state('order.detail', {
            url: "/detail/:id",
            controller: 'OrderDetailController',
            controllerAs: 'detail',
            templateUrl: 'orderDetail.html',
            resolve: {
              orderData: function(Orders, $stateParams) {
                return Orders.get({'id': $stateParams.id}).$promise;
              }
            }
          })

          .state('order.create', {
            url: "/create",
            controller: 'OrderCreateController',
            controllerAs: 'create',
            templateUrl: 'orderCreate.html',
            resolve: {
              orderData: function(Orders) {
                return new Orders();
              }
            },
            data: {
              rolesOnly: ['ROLE_OPERATOR']
            }
          })

          .state('error403', {
            url: 'e403',
            template: '<h1>ERROR 403</h1>'
          })

          .state('error404', {
            url: 'e404',
            template: '<h1>ERROR 404</h1>'
          });
    })

    .config(function($httpProvider) {

      $httpProvider.interceptors.push(function($injector) {

        return {
          responseError: function(response) {
            return $injector.invoke(function(loginModal, $http, $rootScope) {

              if (response.status === 401) {
                if (response.data.path !== '/login') {
                  return loginModal.getLoginModal()
                      .then(function() {
                        return $http(response.config);
                      });
                }
              }

              if (response.status === 403) {
                $rootScope.$broadcast('restApi:forbidden');
              }

              return response;
            });
          }
        };
      });
    })

    .run(function($rootScope, $state, authService, loginModal) {

      function checkStateRolesWhenRestrictedAndGo(authService, toState, toParams) {

        if (toState.data.rolesOnly) {
          if (!authService.hasSomeRole(toState.data.rolesOnly)) {
            $rootScope.$broadcast('permissionError', 'User has not permissions to ' + toState.name);
            return;
          }
        }
        $state.go(toState, toParams, {notify: false});
      }

      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        if (!toState.hasOwnProperty('data')) {  // má stav definovanou položku data
          return;
        }

        var mustBeLogged = toState.data.loggedOnly || toState.data.rolesOnly;
        if (!mustBeLogged) {  // nemusím být přihlášený
          return;
        }

        if (authService.isLogged()) {
          checkStateRolesWhenRestrictedAndGo(authService, toState, toParams);
        } else {
          event.preventDefault(); // zastavit routing
          loginModal.getLoginModal().then(function(auth) {
            checkStateRolesWhenRestrictedAndGo(auth, toState, toParams);
          });
        }
      });
    })

    .run(function($rootScope, $state) {
      $rootScope.$on('restApi:forbidden', function() {
        console.log('restApi:forbidden', arguments);
        $state.go('error403');
      });
      $rootScope.$on('authService:loginFailed', function() {
        console.log('authService:loginFailed');
      });
      $rootScope.$on('routingFailed:notAuthorized', function(event) {
        console.log('routingFailed:notAuthorized', arguments);
        $state.go('error404');
      });
      $rootScope.$on('$routeChangeError', function(event) {
        console.log('$routeChangeError', arguments);
        $state.go('error404');
      });
      $rootScope.$on('permissionError', function(event) {
        console.log('error404', arguments);
        $state.go('error404');
      });
      $rootScope.$on('$stateNotFound', function(event) {
        console.log('$stateNotFound', arguments);
        $state.go('error404');
      });
      $rootScope.$on('$stateChangeError', function(event) {
        console.log('$stateChangeError', arguments);
        $state.go('error404');
      });
    });


