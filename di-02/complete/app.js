'use strict';

angular.module('diServices', [])
  .provider('calculator', function () {
    var pagePrice = 3;

    this.setPagePrice = function(price) {
      pagePrice = price;
    };

    this.$get = function (logger) {
      return {
        getPrice: function (product) {

          var price = 0;
          var baseCoverPrice = 70;
          switch (product.pageSize) {
            case 'A6':
              price += baseCoverPrice;
              break;
            case 'A5':
              price += baseCoverPrice + 20;
              break;
            case 'A4':
              price += baseCoverPrice + 40;
              break;
          }

          var pagesPrice = Math.ceil(product.numberOfPages / 5) * pagePrice;
          price += pagesPrice;

          logger.log('Price : ' + price + " - " + product.numberOfPages + " stran, " + product.pageSize);
          return price;
        }
      };
    };
  })

  .service('logger', function () {
    this.log = function (message) {
      console.log(message);
    };
  });

angular.module('diApp', ['diServices'])
  .constant('PAGE_PRICE', 4)

  .config(function(calculatorProvider, PAGE_PRICE){
    calculatorProvider.setPagePrice(PAGE_PRICE);
  })
  
  .value('defaultProduct', {
    pageSize: 'A5',
    numberOfPages: 50
  })

  .controller('CalculatorCtrl', function (defaultProduct, calculator) {
    this.product = defaultProduct;

    this.getPrice = function () {
      return calculator.getPrice(this.product);
    };
  });